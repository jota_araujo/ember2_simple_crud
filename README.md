# Description:

A CRUD demo application using [Ember.js 2.5](http://emberjs.com/) and [Semantic-UI](http://semantic-ui.com/).

For another version of the same app using [Vue.js](http://vuejs.org) see this link:
(https://bitbucket.org/jota_araujo/vue_js_crud_demo)

# Screenshots:

![Product List Page](https://bytebucket.org/jota_araujo/ember2_simple_crud/raw/337100e6420f64b71a7093f1f3acb64b668e90d2/ember_client_1.jpg)

![Product Edit Page]
(https://bytebucket.org/jota_araujo/ember2_simple_crud/raw/337100e6420f64b71a7093f1f3acb64b668e90d2/ember_client_2.jpg)