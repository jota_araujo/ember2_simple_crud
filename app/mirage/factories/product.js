import Mirage from 'ember-cli-mirage';

export default Mirage.Factory.extend(
  {name: 'MyString', priceCents: 'MyString', marginCents: 42, comission: 42, expiration: new Date(), trackStock: false, stockAmount: 42 }
);
