//import Model from 'ember-data/model';
//import attr from 'ember-data/attr';
import DS from 'ember-data';

const { attr, hasMany } = DS;

export default DS.Model.extend({
  name: attr('string'),
  products: hasMany('product')
});
