//import Model from 'ember-data/model';
//import attr from 'ember-data/attr';
import DS from 'ember-data';

const { attr, belongsTo } = DS;

export default DS.Model.extend({
  name: attr('string'),
  priceCents: attr('number'),
  marginCents: attr('number'),
  commission: attr('number'),
  expiration: attr('date'),
  trackStock: attr('boolean'),
  stockAmount: attr('number'),

  formattedPrice: Ember.computed('priceCents', function() {
  	return (this.get('priceCents')/100).toFixed(2);
  }),

  brand: belongsTo('brand'),
  group: belongsTo('product-group')

});
