import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType
});

Router.map(function() {
  this.route('brands', function() {
    this.route('new');

    this.route('edit', {
      path: ':brand_id/edit'
    });

    this.route('show', {
      path: ':brand_id'
    });
  });
  this.route('products', function() {
    this.route('new');

    this.route('edit', {
      path: ':product_id/edit'
    });

    this.route('show', {
      path: ':product_id'
    });
  });
  this.route('product-groups', function() {
    this.route('new');

    this.route('edit', {
      path: ':product-group_id/edit'
    });

    this.route('show', {
      path: ':product-group_id'
    });
  });
});

export default Router;
