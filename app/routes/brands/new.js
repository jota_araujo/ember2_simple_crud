import Ember from 'ember';
import SaveModelMixin from 'app4/mixins/brands/save-model-mixin';

export default Ember.Route.extend(SaveModelMixin, {
  model: function() {
    return this.store.createRecord('brand');
  }
});
