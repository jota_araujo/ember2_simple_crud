import Ember from 'ember';

export default Ember.Route.extend({
  actions: {
    remove: function(model) {
      if(confirm('Are you sure?')) {
        model.destroyRecord();
      }
    },

    updateList: function(component, id, value) {
        console.log('selected brand:', id);
      }
  },
 
  model: function() {
    return Ember.RSVP.hash({
      products: this.store.query('product', { order_by: "id", sort_direction: "asc"}),
      brands: this.store.findAll("brand")
    });
  }
});
