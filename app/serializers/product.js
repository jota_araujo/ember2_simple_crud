import Ember from 'ember';
import DS from 'ember-data';


export default DS.RESTSerializer.extend(DS.EmbeddedRecordsMixin, {

  attrs: {
    brand: { embedded: 'always' },
    group: { embedded: 'always' }
  },

  extractMeta: function(store, typeClass, payload) {
    let meta = payload.meta;
    console.log(meta);
    return meta;
  },

  keyForAttribute: function(attr, method) {
	    return Ember.String.underscore(attr);
	  },
  keyForRelationship: function(key, relationship, method) {

  	if(key === 'product-group' && relationship === "belongsTo"){
  		//console.log("match for: " + key + " " + relationship);
  		return 'productGroup';
  	}
  	return Ember.String.underscore(key);
   }


});