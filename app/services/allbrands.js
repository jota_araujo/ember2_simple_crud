import Ember from 'ember';

export default Ember.Service.extend({
  store: Ember.inject.service(),

  items: null,

  init() {
    this.items =  this.get('store').findAll("brand");
  }

});