import Ember from 'ember';
import { module, test } from 'qunit';
import startApp from '../helpers/start-app';

var application;
var originalConfirm;
var confirmCalledWith;

module('Acceptance: Brand', {
  beforeEach: function() {
    application = startApp();
    originalConfirm = window.confirm;
    window.confirm = function() {
      confirmCalledWith = [].slice.call(arguments);
      return true;
    };
  },
  afterEach: function() {
    Ember.run(application, 'destroy');
    window.confirm = originalConfirm;
    confirmCalledWith = null;
  }
});

test('visiting /brands without data', function(assert) {
  visit('/brands');

  andThen(function() {
    assert.equal(currentPath(), 'brands.index');
    assert.equal(find('#blankslate').text().trim(), 'No Brands found');
  });
});

test('visiting /brands with data', function(assert) {
  server.create('brand');
  visit('/brands');

  andThen(function() {
    assert.equal(currentPath(), 'brands.index');
    assert.equal(find('#blankslate').length, 0);
    assert.equal(find('table tbody tr').length, 1);
  });
});

test('create a new brand', function(assert) {
  visit('/brands');
  click('a:contains(New Brand)');

  andThen(function() {
    assert.equal(currentPath(), 'brands.new');

    fillIn('label:contains(Name) input', 'MyString');

    click('input:submit');
  });

  andThen(function() {
    assert.equal(find('#blankslate').length, 0);
    assert.equal(find('table tbody tr').length, 1);
  });
});

test('update an existing brand', function(assert) {
  server.create('brand');
  visit('/brands');
  click('a:contains(Edit)');

  andThen(function() {
    assert.equal(currentPath(), 'brands.edit');

    fillIn('label:contains(Name) input', 'MyString');

    click('input:submit');
  });

  andThen(function() {
    assert.equal(find('#blankslate').length, 0);
    assert.equal(find('table tbody tr').length, 1);
  });
});

test('show an existing brand', function(assert) {
  server.create('brand');
  visit('/brands');
  click('a:contains(Show)');

  andThen(function() {
    assert.equal(currentPath(), 'brands.show');

    assert.equal(find('p strong:contains(Name:)').next().text(), 'MyString');
  });
});

test('delete a brand', function(assert) {
  server.create('brand');
  visit('/brands');
  click('a:contains(Remove)');

  andThen(function() {
    assert.equal(currentPath(), 'brands.index');
    assert.deepEqual(confirmCalledWith, ['Are you sure?']);
    assert.equal(find('#blankslate').length, 1);
  });
});
