import Ember from 'ember';
import { module, test } from 'qunit';
import startApp from '../helpers/start-app';

var application;
var originalConfirm;
var confirmCalledWith;

module('Acceptance: Product-group', {
  beforeEach: function() {
    application = startApp();
    originalConfirm = window.confirm;
    window.confirm = function() {
      confirmCalledWith = [].slice.call(arguments);
      return true;
    };
  },
  afterEach: function() {
    Ember.run(application, 'destroy');
    window.confirm = originalConfirm;
    confirmCalledWith = null;
  }
});

test('visiting /product-groups without data', function(assert) {
  visit('/product-groups');

  andThen(function() {
    assert.equal(currentPath(), 'product-groups.index');
    assert.equal(find('#blankslate').text().trim(), 'No Product-groups found');
  });
});

test('visiting /product-groups with data', function(assert) {
  server.create('product-group');
  visit('/product-groups');

  andThen(function() {
    assert.equal(currentPath(), 'product-groups.index');
    assert.equal(find('#blankslate').length, 0);
    assert.equal(find('table tbody tr').length, 1);
  });
});

test('create a new product-group', function(assert) {
  visit('/product-groups');
  click('a:contains(New Product-group)');

  andThen(function() {
    assert.equal(currentPath(), 'product-groups.new');

    fillIn('label:contains(Name) input', 'MyString');
    fillIn('label:contains(Products) input', 'MyString');

    click('input:submit');
  });

  andThen(function() {
    assert.equal(find('#blankslate').length, 0);
    assert.equal(find('table tbody tr').length, 1);
  });
});

test('update an existing product-group', function(assert) {
  server.create('product-group');
  visit('/product-groups');
  click('a:contains(Edit)');

  andThen(function() {
    assert.equal(currentPath(), 'product-groups.edit');

    fillIn('label:contains(Name) input', 'MyString');
    fillIn('label:contains(Products) input', 'MyString');

    click('input:submit');
  });

  andThen(function() {
    assert.equal(find('#blankslate').length, 0);
    assert.equal(find('table tbody tr').length, 1);
  });
});

test('show an existing product-group', function(assert) {
  server.create('product-group');
  visit('/product-groups');
  click('a:contains(Show)');

  andThen(function() {
    assert.equal(currentPath(), 'product-groups.show');

    assert.equal(find('p strong:contains(Name:)').next().text(), 'MyString');
    assert.equal(find('p strong:contains(Products:)').next().text(), 'MyString');
  });
});

test('delete a product-group', function(assert) {
  server.create('product-group');
  visit('/product-groups');
  click('a:contains(Remove)');

  andThen(function() {
    assert.equal(currentPath(), 'product-groups.index');
    assert.deepEqual(confirmCalledWith, ['Are you sure?']);
    assert.equal(find('#blankslate').length, 1);
  });
});
